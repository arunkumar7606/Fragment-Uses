package com.arun.aashu.assignmentoffragment;


import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Calendar;


/**
 * A simple {@link Fragment} subclass.
 */
public class BlankFragment extends Fragment {

    View vi;
    int i,dayOfMonth;
    public BlankFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

       vi= inflater.inflate(R.layout.fragment_blank, container, false);

        EditText ed=vi.findViewById(R.id.vehicleNumber);
        EditText ed2=vi.findViewById(R.id.datetext);
        Button  b=vi.findViewById(R.id.check);
        final TextView tv=vi.findViewById(R.id.permissionText);

       ed2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Calendar ca=Calendar.getInstance();

                new DatePickerDialog(getContext(),dateSetListener,
                        ca.get(Calendar.YEAR),ca.get(Calendar.MONTH),
                        ca.get(Calendar.DAY_OF_MONTH)).show();

            }
        });
       String s1=ed.getText().toString();

//       i=Integer.parseInt(s1);

        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (i%2==0&&dayOfMonth<16){
                        tv.setText("you can drive");
                }
                else if(i%2==1&&dayOfMonth>15){
                    tv.setText("you can drive");

                }else if (i%2==0&&dayOfMonth>15){
                    tv.setText("you can not drive");
                }
                else if (i%2==1&&dayOfMonth<16){
                    tv.setText("you can not drive");
                }
            }
        });



        // Inflate the layout for this fragment
        return vi;
    }
    DatePickerDialog.OnDateSetListener dateSetListener=new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker datePicker, int year , int month, int dayOfMonth) {

            
          EditText ed2=  vi.findViewById(R.id.datetext);
                    ed2.setText(dayOfMonth +"/"+month+"/"+year);

        }
    };



}
